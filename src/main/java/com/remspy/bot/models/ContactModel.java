package com.remspy.bot.models;

import com.remspy.bot.net.annotations.TransferField;
import com.remspy.bot.net.enums.TransferFieldTypes;

import java.util.ArrayList;

public class ContactModel {

    @TransferField(type = TransferFieldTypes.Int, order = 0)
    private int id;

    @TransferField(type = TransferFieldTypes.ChunkString, order = 1)
    private String name;

    @TransferField(type = TransferFieldTypes.ChunkString, order = 2)
    private String note;

    @TransferField(type = TransferFieldTypes.List, order = 3)
    private ArrayList<ContactGroupModel> contactAddresses;

    @TransferField(type = TransferFieldTypes.List, order = 4)
    private ArrayList<ContactGroupModel> contactPhones;

    @TransferField(type = TransferFieldTypes.List, order = 5)
    private ArrayList<ContactGroupModel> contactEmails;

    @TransferField(type = TransferFieldTypes.List, order = 6)
    private ArrayList<ContactGroupModel> contactIMs;

    public int getId() {
        return id;
    }

    public ContactModel setId(int id) {
        this.id = id;
        return this;
    }

    public ContactModel setName(String name) {
        this.name = name;
        return this;
    }

    public String getName() {
        return name;
    }

    public ContactModel setNote(String note) {
        this.note = note;
        return this;
    }

    public String getNote() {
        return note;
    }

    public ContactModel setContactAddresses(ArrayList<ContactGroupModel> contactAddresses) {
        this.contactAddresses = contactAddresses;
        return this;
    }

    public ContactModel setContactPhones(ArrayList<ContactGroupModel> contactPhones) {
        this.contactPhones = contactPhones;
        return this;
    }

    public ContactModel setContactEmails(ArrayList<ContactGroupModel> contactEmails) {
        this.contactEmails = contactEmails;
        return this;
    }

    public ContactModel setContactIMs(ArrayList<ContactGroupModel> contactIMs) {
        this.contactIMs = contactIMs;
        return this;
    }

}
