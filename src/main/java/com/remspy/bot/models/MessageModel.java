package com.remspy.bot.models;

import com.remspy.bot.net.annotations.TransferField;
import com.remspy.bot.net.enums.TransferFieldTypes;

public class MessageModel {

    @TransferField(type = TransferFieldTypes.Int, order = 0)
    private int id;

    @TransferField(type = TransferFieldTypes.ChunkString, order = 1)
    private String address;

    @TransferField(type = TransferFieldTypes.ChunkString, order = 2)
    private String body;

    @TransferField(type = TransferFieldTypes.Byte, order = 3)
    private byte type;

    public int getId() {
        return id;
    }

    public MessageModel setId(int id) {
        this.id = id;
        return this;
    }

    public MessageModel setType(byte type) {
        this.type = type;
        return this;
    }

    public MessageModel setAddress(String address) {
        this.address = address;
        return this;
    }

    public MessageModel setBody(String body) {
        this.body = body;
        return this;
    }

}
