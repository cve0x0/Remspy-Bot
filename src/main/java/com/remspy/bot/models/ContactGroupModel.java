package com.remspy.bot.models;

import com.remspy.bot.net.annotations.TransferField;
import com.remspy.bot.net.enums.TransferFieldTypes;

public class ContactGroupModel {

    @TransferField(type = TransferFieldTypes.ChunkString, order = 0)
    private String data;

    @TransferField(type = TransferFieldTypes.ChunkString, order = 1)
    private String type;

    public ContactGroupModel setData(String data) {
        this.data = data;
        return this;
    }

    public ContactGroupModel setType(String type) {
        this.type = type;
        return this;
    }

    public String getData() {
        return data;
    }

    public String getType() {
        return type;
    }

}
