package com.remspy.bot.models;

public class PhotoModel {

    private int id;

    private int blockRecordId;

    private long timestamp;

    private boolean haveLocations;

    private double latitude;

    private double longitude;

    public int getId() {
        return id;
    }

    public PhotoModel setId(int id) {
        this.id = id;
        return this;
    }

    public int getBlockRecordId() {
        return blockRecordId;
    }

    public PhotoModel setBlockRecordId(int blockRecordId) {
        this.blockRecordId = blockRecordId;
        return this;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public PhotoModel setTimestamp(long timestamp) {
        this.timestamp = timestamp;
        return this;
    }

    public boolean isHaveLocations() {
        return haveLocations;
    }

    public PhotoModel setHaveLocations(boolean haveLocations) {
        this.haveLocations = haveLocations;
        return this;
    }

    public double getLatitude() {
        return latitude;
    }

    public PhotoModel setLatitude(double latitude) {
        this.latitude = latitude;
        return this;
    }

    public double getLongitude() {
        return longitude;
    }

    public PhotoModel setLongitude(double longitude) {
        this.longitude = longitude;
        return this;
    }

}
