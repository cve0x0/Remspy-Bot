package com.remspy.bot.net.interfaces;

import com.remspy.bot.net.BufferStream;

public interface DeserializePackage {

    void onDeserialize(BufferStream buffer) throws Exception;

}
