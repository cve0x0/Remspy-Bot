package com.remspy.bot.net.interfaces;

import com.remspy.bot.net.BufferStream;

public interface SerializablePackage {

    void onSerialize(BufferStream buffer) throws Exception;

}
