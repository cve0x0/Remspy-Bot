package com.remspy.bot.net;

import com.remspy.bot.net.interfaces.DeserializePackage;
import com.remspy.bot.net.interfaces.SerializablePackage;

import java.util.ArrayList;

public class TransferList<T> extends ArrayList<T> implements SerializablePackage, DeserializePackage {

    private Class<T> mClass;

    public TransferList() {

    }

    public TransferList(Class<T> cls) {
        mClass = cls;
    }

    @Override
    public void onSerialize(BufferStream buffer) throws Exception {
        buffer.putTransferList(this);
    }

    @Override
    public void onDeserialize(BufferStream buffer) throws Exception {
        addAll(buffer.getTransferList(mClass));
    }

}
