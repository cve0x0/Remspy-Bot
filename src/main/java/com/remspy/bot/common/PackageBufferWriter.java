package com.remspy.bot.common;

import com.remspy.bot.constants.ProtocolConstants;
import com.remspy.bot.net.PackageWriter;
import com.remspy.bot.net.utils.BufferUtils;

import java.nio.ByteBuffer;

public class PackageBufferWriter extends PackageWriter {

    public PackageBufferWriter(int size) {
        super(size);
    }

    public PackageBufferWriter(int size, boolean havePackageId) {
        super(size, havePackageId);
    }

    @Override
    protected ByteBuffer onBufferInit() {
        mHeaderSize += 1;

        ByteBuffer buffer = BufferUtils.allocate(mHeaderSize + mBufferSize);
        buffer.put(ProtocolConstants.BUFFER_START);

        return buffer;
    }

}
